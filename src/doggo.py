from datetime import datetime

def get_timestamp():
    return datetime.now().strftime(("%Y-%m-%d %H:%M:%S"))

# Data to serve with our API
DOGGO = {
    "breed": "Akita",
    "img-path": "/somewhere/",
    "personality": "Suplado",
    "ideal-owners": "akind",
    "care-reqs": "love",
    "fun-fact": "it is blah"
}

# Create a handler for our read (GET) doggo
def read():
    """
    This function responds to a request for /api/doggo-random

    # Create the list of doggo from our data
    """
    return DOGGO