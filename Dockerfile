FROM python:alpine 

LABEL maintainer="Nikkorenz Clarin"

RUN pip install flask
RUN pip install connexion[swagger-ui]

COPY src /src/

EXPOSE 5000

ENTRYPOINT ["connexion", "run", "/src/swagger.yml"]